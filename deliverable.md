This is the deliverable instructions file containing the required topics.
Due to the lack of experience with the systems, not all tasks have been completed.

# Git repository with infra code, deployment
The code and contents of the repository are available on [GitLab](https://gitlab.com/JAVENINSJ/spinwise_test_project)

This contains the yaml definitions for grafana, prometheus and stress-app.

## Prerequesites
The required tools are the following: kubectl, kind, argocd cli, helm

To install these on a Linux instance, run `./helper_scripts/install_requirements.sh`

*Note:* The scripts mentioned in this repository were designed around a Linux Ubuntu instance and have not been tested on other operating systems. Your milage may vary.

# How to deploy the cluster & How to access monitoring system - url, credentials, etc.
To automate deployment and make things smoother, I have created a couple of helper scripts, which can be run, to automate instalation of the tools and environment creation.

To launch them in the correct order, use the 'deploy.sh' script by running `./deploy.sh` from the dir of the repository. This will install, and launch ArgoCD, Prometheus, Grafana.

All of the credentials and access links will be displayed in the terminal window.

*Note:* The scripts mentioned in this repository were designed around a Linux Ubuntu instance and have not been tested on other operating systems. Your milage may vary.

## Default values
ArgoCD - http://localhost:8080
admin:<password from terminal>

Prometheus - http://localhost:9090

Grafana - http://localhost:3000
admin:admin

# Any extra configuration or steps required to launch the cluster
To launch the stress job, run `./test.sh`.
This will deploy the stressable app to ArgoCD.