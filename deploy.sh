#! /bin/bash
set -e

./helper_scripts/deploy_argocd.sh
./helper_scripts/deploy_monitoring.sh
./helper_scripts/deploy_dashboards.sh
