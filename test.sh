#! /bin/bash
set -e

gitlab_url=${gitlab_url:-https://gitlab.com/JAVENINSJ/spinwise_test_project.git}

kubectl create namespace stress

argocd app create stress-app \
  --repo $gitlab_url \
  --path stress-job \
  --dest-server https://kubernetes.default.svc \
  --dest-namespace stress

argocd app sync stress-app >/dev/null 2>&1