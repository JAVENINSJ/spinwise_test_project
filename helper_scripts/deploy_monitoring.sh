#! /bin/bash
set -e

monitoring_namespace=${monitoring_namespace:-monitoring}
gitlab_url=${gitlab_url:-https://gitlab.com/JAVENINSJ/spinwise_test_project.git}

## Set the provided variables
while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
    shift
done

## Check if the monitoring namespace exists
echo "Ensuring namespace for monitoring..."
if kubectl get namespace $monitoring_namespace &>/dev/null; then
    echo "Namespace '$monitoring_namespace' already exists."
else
    kubectl create namespace $monitoring_namespace >/dev/null 2>&1
    echo "Namespace '$monitoring_namespace' created."
fi

## Installing the monitoring stack
argocd app create prometheus \
  --repo $gitlab_url \
  --path prometheus \
  --dest-server https://kubernetes.default.svc \
  --dest-namespace monitoring

argocd app create grafana \
  --repo $gitlab_url \
  --path grafana \
  --dest-server https://kubernetes.default.svc \
  --dest-namespace monitoring

argocd app sync prometheus >/dev/null 2>&1
argocd app sync grafana >/dev/null 2>&1