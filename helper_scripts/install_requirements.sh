#! /bin/bash
set -e

## kubectl
if which kubectl >/dev/null 2>&1; then
    echo "kubectl is already installed"
else
    echo "Installing kubectl..."
    sys_type=$(uname -m)
    if [ "$sys_type" == "x86_64" ]; then
        curl -sSLO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    elif [ "$sys_type" == "aarch64" ]; then
        curl -sSLO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
    else
        echo "Unsupported architecture: $sys_type"
        exit 1
    fi

    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    echo "kubectl installed!"
fi


## kind
if which kind >/dev/null 2>&1; then
    echo "kind is already installed"
else
    echo "Installing kind..."

    if [ "$sys_type" == "x86_64" ]; then
        curl -sSL -o ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
    # For ARM64
    elif [ "$sys_type" == "aarch64" ]; then
        curl -sSL -o ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-arm64
    else
        echo "Unsupported architecture: $sys_type"
        exit 1
    fi

    sudo install -o root -g root -m 0755 kind /usr/local/bin/kind
    echo "kind installed!"
fi


## ArgoCD CLI
if which argocd >/dev/null 2>&1; then
    echo "argocd is already installed"
else
    echo "Installing argocd..."

    if [ "$sys_type" == "x86_64" ]; then
        curl -sSL -o ./argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
    # For ARM64
    elif [ "$sys_type" == "aarch64" ]; then
        curl -sSL -o ./argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-arm64
    else
        echo "Unsupported architecture: $sys_type"
        exit 1
    fi

    sudo install -o root -g root -m 0755 argocd /usr/local/bin/argocd
    echo "argocd installed!"
fi


## Helm
if which helm >/dev/null 2>&1; then
    echo "helm is already installed"
else
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh
    rm ./get_helm.sh
fi