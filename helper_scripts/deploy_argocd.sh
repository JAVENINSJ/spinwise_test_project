#! /bin/bash
set -e

cluster_name=${cluster_name:-kind-cluster}
cluster_config=${cluster_config:-cluster-config.yaml}

argocd_git="https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml"
argocd_namespace=${argocd_namespace:-argocd}

## Set the provided variables
while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
    shift
done

## Check if cluster already exists
echo "Ensuring cluster is defined..."
if kind get clusters | grep -q "$cluster_name"; then
    echo "Cluster '$cluster_name' exists!"
else
    echo "Creating '$cluster_name' from '$cluster_config'..."
    kind create cluster --name "$cluster_name" --config "$cluster_config"
fi
echo

## Check if the argocd namespace exists
echo "Ensuring namespace for ArgoCD..."
if kubectl get namespace $argocd_namespace &>/dev/null; then
    echo "Namespace '$argocd_namespace' already exists."
else
    kubectl create namespace $argocd_namespace
    echo "Namespace '$argocd_namespace' created."
fi
echo

## Install argocd
if kubectl get deployment -n argocd argocd-server &>/dev/null; then
    echo "ArgoCD is already installed."
else
    echo "Installing ArgoCD..."
    kubectl apply -n $argocd_namespace -f $argocd_git >/dev/null 2>&1
fi

admin_pass=$(kubectl get secret \
  -n $argocd_namespace \
  argocd-initial-admin-secret \
  -o jsonpath="{.data.password}" |
  base64 -d)
echo

kubectl patch svc argocd-server -n "argocd" -p \
  '{"spec": {"type": "NodePort", "ports": [{"name": "http", "nodePort": 30080, "port": 80, "protocol": "TCP", "targetPort": 8080}, {"name": "https", "nodePort": 30443, "port": 443, "protocol": "TCP", "targetPort": 8080}]}}' >/dev/null 2>&1

admin_pass=$(kubectl get secret \
  -n argocd \
  argocd-initial-admin-secret \
  -o jsonpath="{.data.password}" |
  base64 -d)
echo "ArgoCD default credentials are admin:$admin_pass"
echo -e "'argocd' available on https://localhost:8080..."

echo

echo "Logging into ArgoCD CLI using the initial password..."
argocd login localhost:8080 --insecure --username admin --password $admin_pass >/dev/null 2>&1

echo "Adding GitLab repository to ArgoCD..."
argocd repo add https://gitlab.com/JAVENINSJ/spinwise_test_project.git

echo -e "\nCluster launched succesfully! ArgoCD deployed!\n"