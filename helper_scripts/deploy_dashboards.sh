#!/bin/bash

start_port_forward() {
    local service_name=$1
    local local_port=$2
    local target_port=$3
    local namespace=$4

    echo "'$service_name' available on http://localhost:$local_port..."
    kubectl port-forward svc/$service_name -n $namespace $local_port:$target_port >/dev/null 2>&1 &
}

echo 

echo "grafana default credentials are admin:admin"
start_port_forward grafana 3000 3000 monitoring
start_port_forward prometheus 9090 80 monitoring

echo 

echo "Port forwarding is running. Press Ctrl+C to stop."
read -r -d '' _ </dev/tty