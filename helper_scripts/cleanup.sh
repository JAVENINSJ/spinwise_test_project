#!/bin/bash

# Function to kill processes using a specific port
kill_process_using_port() {
    local port=$1
    local pid_list=$(lsof -t -i :$port)
    
    if [ -n "$pid_list" ]; then
        echo "Processes using port $port:"
        echo "$pid_list"
        echo "Killing processes..."
        for pid in $pid_list; do
            kill $pid
            echo "Killed process with PID $pid"
        done
    else
        echo "No processes found using port $port."
    fi
}

# Function to remove files matching pattern
remove_files_matching_pattern() {
    local pattern=$1
    local files=$(find . -name "$pattern")
    
    if [ -n "$files" ]; then
        echo "Removing files matching pattern $pattern:"
        echo "$files"
        rm -f $files
        echo "Files removed."
    else
        echo "No files found matching pattern $pattern."
    fi
}

kill_process_using_port 9090

kill_process_using_port 3000

remove_files_matching_pattern "*_port_forward.log"